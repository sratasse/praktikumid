package praktikum11;

public class rekursiiv {

	public static void main(String[] args) {
		
		System.out.println(astenda(2, 4));
		
			
		

	}

	public static int astenda(int i, int j) {
		if(j == 1){
			return i;
		}
		return i * astenda(i, j - 1);
		
	}

}
//vastus: a* a�b-1
//16
//(2, 4) { 2 * 8}
// (2, 3){2 * 4}
// (2 ,2) {2 * 2}
// (2, 1){2}

//�lesanne oli, et kirjutada meetod, mis arvutab
//rekursiivselt t�isarvu positiivse t�isarvulise astme.