package praktikum11;

public class fibonacci {

	public static void main(String[] args) {
		System.out.println(fibb(6));

	}

	public static int fibb(int a) {
		if (a <= 1 ){
			return a;
		}
		return fibb(a-1) + fibb(a - 2);
	}

}
