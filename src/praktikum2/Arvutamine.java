
package praktikum2;

import lib.TextIO;
import sun.security.action.GetLongAction;

public class Arvutamine {

	public static void main(String[] args) {

		// defineerime muutujad
		int arv1;
		int arv2;
		int korrutis;

		// küsime kasutajalt 2 arvu
		System.out.println("Palun sisesta kaks arvu");
		// väärtustame muutujad
		arv1 = TextIO.getInt();
		arv2 = TextIO.getInt();

		// arvutame korrutise
		korrutis = arv1 + arv2;

		// väljastame tulemuse
		System.out.println("Nende arvude tehe on: " + korrutis);

	}

}
