package praktikum10;

import java.util.ArrayList;
import java.util.Scanner;

public class Maatriksiga {

	public static <E> void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		ArrayList numbrid = new ArrayList();
		int suurim = Integer.MIN_VALUE;
		while (true) {
			System.out.println("Tell me a number");
			int input = sc.nextInt();
			if (input == 0){
				break;
			}
			if(input > suurim)
				suurim = input;
			numbrid.add(input);
		
		}
		double kordaja = 10.0 / suurim;
		for (int i = 0; i < numbrid.size(); i++) {
			int nr = (int) numbrid.get(i);
			System.out.printf("%2d %s\n", numbrid.get(i), genereeriIksid(nr, kordaja));
		}
	
	}

	

	public static String genereeriIksid(int nr, double kordaja) {
		String Maatriksiga = "";
		for (int i = 0; i < nr * kordaja; i++) {
			Maatriksiga = Maatriksiga + "x";
		}
		return Maatriksiga;
	}
	
}
