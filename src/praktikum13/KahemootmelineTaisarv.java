package praktikum13;

public class KahemootmelineTaisarv {

	public static void main(String[] args) {
		int[] rida = { 4, 5, 7, 3 };
		int[][] maatriks = { { 4, 6, 3, 0 }, { 4, 2, 9, 1 }, { 6, 3, 1, 0 } };
		int ridu = 3;
		int veerge = 5;
		

		peaDiagonaaliSumma(maatriks);
		ridadeMaksimumid(maatriks);
		miinimum(maatriks);
		kahegaJaakMaatriks(ridu, veerge);

	}

	/*
	 * Kirjutada meetod, mis tr�kib ekraanile �hel real parameetrina etteantud
	 * �hem��tmelise t�isarvumassiivi elemendid
	 */
	public static void tryki(int[] rida) {
		for (int arv : rida) {
			System.out.print(arv + " ");
		}
		System.out.println();
	}

	/*
	 * Kirjutada meetod, mis tr�kib ekraanile parameetrina etteantud
	 * kahem��tmelise massiivi (maatriksi)
	 */
	public static void tryki(int[][] maatriks) {
		for (int[] rida : maatriks) {
			tryki(rida);
		}
	}

	/*
	 * Arvutada maatriksi iga rea elementide summa
	 */
	public static int[] ridadeSummad(int[][] maatriks) {

		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}

		return summad;
	}

	public static int reaSumma(int[] rida) {
		int summa = 0;
		for (int i = 0; i < rida.length; i++) {
			summa = rida[i] + summa;
		}
		return summa;
	}

	/*
	 * Arvutada k�rvaldiagonaali elementide summa (k�rvaldiagonaal on see, mis
	 * jookseb �levalt paremast nurgast alla vasakusse nurka).
	 */
	public static int peaDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;

		for (int i = 0; i < maatriks.length; i++) {
			summa = summa + maatriks[i][i];
		}
		System.out.println("Peadiagonaalide summa on " + summa);

		return summa;

	}

	/*
	 * Leida iga rea suurim element.
	 */
	public static int[] ridadeMaksimumid(int[][] maatriks) {

		int[] max = new int[maatriks.length];

		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] > max[i]) {
					max[i] = maatriks[i][j];
				}
			}
		}
		System.out.print("Iga rea maksimumid on: ");

		for (int i = 0; i < maatriks.length; i++) {

			System.out.print(max[i] + " ");
		}
		System.out.println();

		return max;
	}

	/*
	 * Leida kogu maatriksi v�ikseim element.
	 */
	public static int miinimum(int[][] maatriks) {
		int min = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maatriks[i][j] < min) {
					min = maatriks[i][j];
				}
			}
		}
		System.out.println("V�ikseim arv on " + min);

		return min;

	}
	/*
	 * Kirjutada programm, mis genereerib parameetritena etteantud suurusega
	 * maatriksi, kus iga element on rea ja veeruindeksi summa kahega jagamise
	 * j��k. Indeksid algavad nullist.
	 */
	public static int[][] kahegaJaakMaatriks(int ridu, int veerge) {
		
		
		
		
		
		return null;
	}

}


