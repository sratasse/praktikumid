package praktikum13;

import lib.TextIO;

public class TaisarvMeetod {

	public static void main(String[] args) {
		System.out.println("Kui mitu numbrit?");
		int i = TextIO.getInt();
		
		int[] rida = new int[i];
		
		for (int j = 0; j < rida.length; j++) {
			System.out.println(j + 1 + ".");
			rida[j] = TextIO.getlnInt();
		}
		
		Tabel2D(rida);
	}
	
	private static void Tabel2D(int[] rida) {
		for (int j = 0; j < rida.length; j++) {
			System.out.println(rida[j] + " ");
		}
	}

}
