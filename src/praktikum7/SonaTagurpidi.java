package praktikum7;

import lib.TextIO;

public class SonaTagurpidi {

	public static void main(String[] args) {
		System.out.println("Sisesta mingi s6na");
		String sona = TextIO.getlnString();
		
		//String.reverse = new StringBuffer(sona).reverse().toString();
		//System.out.println("Sona tagurpidi " + reverse);
		
		String vastus = new StringBuilder(sona).reverse().toString();
		System.out.println(vastus);
	}

}
