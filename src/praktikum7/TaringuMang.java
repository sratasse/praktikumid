package praktikum7;

import lib.TextIO;

public class TaringuMang {

	public static void main(String[] args) {
		int a = 100;
		
		while(true) {
			a = Mang(a);
			
			if(a <= 0) {
				System.out.println("Sul sai raha otsa. Mang l2bi.");
				break;
			}
		}
	}
	
	public static int Mang(int a) {
		System.out.println("Sul on " + a + " raha.");
		int panus = panustamine(0, a);
		System.out.println("Paku, mis taringu vise v6is tulla (1 - 6)");
		
		int kasutajaArvab = Mangija(0, 6);
		
		int myndiVise = Vastane();
		
		if(myndiVise == kasutajaArvab) {
			System.out.println("Arvasid 2ra");
		} else {
			System.out.println("M88da panid");
			a = a - panus;
		}
		return a;
	}
	
	public static int panustamine(int min, int max) {
		System.out.println("Kui suure panuse tahad teha?");
		while(true) {
			int panus = TextIO.getlnInt();
			if(panus > min && panus <= max) {
				return panus;
			} else if (panus <= max){
				System.out.println("Sa ei panustanud midagi. Sisesta uuesti.");
			} else if (panus > max) {
				System.out.println("Sul pole nii palju raha. Sisesta uuesti.");
			}
		}
	}
	
	public static int Vastane() {
		int b = (int) (Math.random() * 6 + 1);
		
		return b;
	}
	
	public static int Mangija(int min, int max) {
		while(true) {
			int sisestus = TextIO.getlnInt();
			if(sisestus >= min && sisestus <= max) {
				return sisestus;
			} else {
				System.out.println("Sisesta arv uuesti.");
			}
		}
	}

}
