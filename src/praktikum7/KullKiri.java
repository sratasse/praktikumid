package praktikum7;

import praktikum6.Meetodid;

public class KullKiri {

public static void main(String[] args) {
		
		// 0 - kull, 1 - kiri
		
		int kasutajaRaha = 100;
		
		while (kasutajaRaha > 0) {
			System.out.println("Sul on: " + kasutajaRaha + " raha");
			int maxPanus = Math.min(25, kasutajaRaha);
			System.out.println("Sisesta panus (max " + maxPanus + ")");
			int panus = Meetodid.kasutajaSisestus(1, maxPanus);
			kasutajaRaha -= panus;
			int myndiVise = Meetodid.suvalineArv(0, 1);
			if (1 == myndiVise) {
				System.out.println("Tuli kiri, saad panuse topelt tagasi!");
				kasutajaRaha += panus * 2;
			} else {
				System.out.println("Tuli kull, ei saa midagi");
			}
		}
		System.out.println("Raha otsas, mäng läbi!");

	}

}
