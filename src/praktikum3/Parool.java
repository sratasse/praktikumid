package praktikum3;

import lib.TextIO;

public class Parool {

	public static void main(String[] args) {

		String oigeParool = "SalaKala";
		
		while (true) {
			System.out.println("Enter Password:");
			String sisestatudParool = TextIO.getlnString();
			if (oigeParool.equals(sisestatudParool)) {
				System.out.println("Correct Password!");
				break;
			} else {
				System.out.println("Wrong Password, guess again!");
			}
		}
	}

}