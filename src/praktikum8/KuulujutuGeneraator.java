package praktikum8;

import lib.TextIO;

public class KuulujutuGeneraator {

	public static void main(String[] args) {
		String nimi1, nimi2, teg3;
		
		String[] naised = new String[3];
		String[] mehed = new String[3];
		String[] tegu = new String[3];
		
		System.out.println("Sisesta naiste nimed.");
		
		for (int i = 0; i < naised.length; i++) {
			System.out.println(i + 1 + ".");
			naised[i] = TextIO.getlnString();
		}
		
		System.out.println("Sisesta meeste nimed.");
		
		for (int i = 0; i < mehed.length; i++) {
			System.out.println(i + 1 + ".");
			mehed[i] = TextIO.getlnString();
		}
		
		System.out.println("Sisesta tegus6na.");
		
		for (int i = 0; i < tegu.length; i++) {
			System.out.println(i + 1 + ".");
			tegu[i] = TextIO.getlnString();
		}
		
		nimi1 = naised[(int) (Math.random() * naised.length)];
		nimi2 = mehed[(int) (Math.random() * mehed.length)];
		teg3 = tegu[(int) (Math.random() * tegu.length)];
		System.out.println(nimi1 + " " + nimi2 + " " + teg3 + " ");
	}

}
