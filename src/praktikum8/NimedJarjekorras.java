package praktikum8;

import java.util.ArrayList;
import java.util.Collections;

import lib.TextIO;

public class NimedJarjekorras {

	public static void main(String[] args) {
		ArrayList<String> nimed = new ArrayList<String>();
		
		System.out.println("Sisesta nimi");
		
		while(true) {
			String sisestus = TextIO.getlnString();
			
			if(sisestus.equals(""))
				break;
			nimed.add(sisestus);
		}
		
		Collections.sort(nimed);
		
		System.out.println("Nimed jarjekorras");
		
		System.out.println(nimed);
	}

}
