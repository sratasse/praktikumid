package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutud {

	public static void main(String[] args) {
		
		String[] mehed = {"Kalle", "Mikk", "Margus"};
		String[] naised = {"Jaanika", "Pille", "Kersti"};
		String[] tegevused = {"jutustavad", "jalutavad", "karjuvad"};
		
		
		System.out.format("%s ja %s %s.", suvalineElement(mehed), suvalineElement(naised), suvalineElement(tegevused));
	}

	public static String suvalineElement(String[] s6nad) {
		int suvalineIndex = Meetodid.suvalineArv(0, s6nad.length - 1);
		return s6nad[suvalineIndex];
	}

}
