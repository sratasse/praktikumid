package praktikum8;

import lib.TextIO;

public class NumbridTagurpidi {

	public static void main(String[] args) {
		int[] numbrid = new int[10];
		int tagurpidi;
		
		System.out.println("Sisestada 10 numbrit");
		
		for (int i = 0; i < numbrid.length; i++) {
			System.out.println(i + 1 + ".");
			numbrid[i] = TextIO.getlnInt();
			
		}
		
		System.out.println("Sisestatud numbrite j2rjekord on: ");
		
		for (int i = 0; i < numbrid.length; i++) {
			System.out.println(numbrid[i] + " ");
		}
		
		System.out.println();
		System.out.println("Numbrid tagurpidi on: ");
		
		for (int i = 0; i < numbrid.length / 2; i++) {
			tagurpidi = numbrid[i];
			numbrid[i] = numbrid[numbrid.length - 1 - i];
			numbrid[numbrid.length - 1 - i] = tagurpidi; 
		}
		
		for (int i = 0; i < numbrid.length; i++) {
			System.out.println(numbrid[i] + " ");
		}
		
	}

}
