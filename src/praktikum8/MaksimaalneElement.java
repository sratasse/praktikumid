package praktikum8;

public class MaksimaalneElement {

	public static void main(String[] args) {
		System.out.println(maxElement1D(new int[] {1, -7, 4, 6, 3}));
		System.out.println(maxElement2D(new int[][] { {3, 4, 2, 5, 6}, {4, 7 ,8 ,8, 9}, {1, 2, 3, 4, 5} }));
	}
	// Meetod, mis leiab yhem66tmelise massiivi maksimumi.
	public static int maxElement1D(int[] massiiv) {
		int max = massiiv[0];
		for(int i : massiiv) {
			if(i > max)
				max = 1;
		}
		return max;
		
	}
	
	public static int maxElement2D(int[][] massiiv) {
		int max = massiiv[0][0];
		for(int[] i : massiiv) {
			if(maxElement1D(i) > max)
				max = maxElement1D(i);
		}
		return max;
		// maatriksi rea maksimumi leidmiseks saad siin edukalt kasutada eelmist
				// meetodit
	}
	

}
