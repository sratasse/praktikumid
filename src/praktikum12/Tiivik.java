package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Tiivik extends Applet {
	public void paint(Graphics g) {
    	// Kysime kui suur aken on?
    	int w = getWidth();
    	int h = getHeight();
    	
        int x0 = w / 2; // Keskpunkt
        int y0 = h / 2;
        int r = Math.max(y0, x0); // Raadius
        int x, y;
        double t;


        // Ta"idame tausta
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);

        // Joonistame
        g.setColor(Color.red);

        for (t = -Math.PI; t < 2 * Math.PI; t = t + Math.PI / 5) {
            x = (int) (r * Math.cos(t) + x0);
            y = (int) (r * Math.sin(t) + y0);
            g.drawLine(x0, y0, x, y);
        }
    }


}
