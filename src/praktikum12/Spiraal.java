package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Spiraal extends Applet {
	
private Graphics g;
    
    public void paint(Graphics g) {
        this.g = g;
        joonistaTaust();
        joonistaSpiraal();
    }
    
    /**
     * Katab tausta valgega
     */
    public void joonistaTaust() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
    
    /**
     * Joonistab Spiraali
     */
    public void joonistaSpiraal() {
        g.setColor(Color.black);
        int keskkohtX = getWidth() / 2;
        int keskkohtY = getHeight() / 2;
        int lastX = 0;
        int lastY = 0;
        
        for (double nurk = 0; nurk <= Math.PI * 25; nurk = nurk + 2.77) {
        		double radius = 10 + nurk * 3;
        	int x = (int) (radius * Math.cos(nurk)) + keskkohtX;
            int y = (int) (radius * Math.sin(nurk)) + keskkohtY;
            
            if(0 != lastX){
            	g.drawLine(x, y, lastX, lastY);
            }
            
            lastX = x;
            lastY = y;
        }
    }
	

}
