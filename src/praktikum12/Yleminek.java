package praktikum12;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Color;

public class Yleminek extends Applet {

	@Override
	public void paint(Graphics g) {

		int w = getWidth();
		int h = getHeight();

		for (int y = 0; y < h; y++) {
			
			
			double concentrate = (double) y / h;
			concentrate = 1 - concentrate;
			
			int a = (int) (concentrate * 255);
			
			
			Color color = new Color (a, a, a); // 0...255
			g.setColor(color);
			g.drawLine(0, y, w, y);{
				
			}
		}
	}

}
