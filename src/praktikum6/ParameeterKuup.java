package praktikum6;

import lib.TextIO;

public class ParameeterKuup {
	
	public static int ruut(int number) {
		return number * number * number;
	}

	public static void main(String[] args) {
		System.out.println("Sisesta arv");
		int arv = TextIO.getlnInt();
		int arvRuudus = ruut(arv);
		System.out.println(arvRuudus);
	}

}
