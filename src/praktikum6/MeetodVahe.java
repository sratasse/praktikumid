package praktikum6;

import lib.TextIO;

public class MeetodVahe {

	public static void main(String[] args) {
		int hinne = kasutajaSisestus(1, 5);
		System.out.println("Kasutaja sisestus " + hinne);
		
	}

	private static int kasutajaSisestus(int max, int min) {
		while(true) {
			System.out.println("Palun sisesta arvu vahemikus " + min + " kuni " + max);
			int sisestus = TextIO.getlnInt();
			if(sisestus >= min && sisestus <= max) {
				return sisestus;
			} else {
				System.out.println("Sisesta arv uuesti");
			}
		}
	}
	

}
