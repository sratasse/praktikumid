package praktikum6;

import lib.TextIO;

public class Meetodid {

	public static int kasutajaSisestus(int min, int max) {
		while (true) {
		System.out.println("Palun sisesta arv vahemikus " + min + " kuni " + max);
		int sisestus = TextIO.getlnInt();
		if (sisestus > min && sisestus <= max) {
			return sisestus;
		} else {
			System.out.println("See arv pole ju 6iges vahemikus");
		}
	
		}
	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return min + (int) (Math.random() * vahemik);
	}
	public static void main(String[] args) {
		int hinne = kasutajaSisestus(1, 5);
		System.out.println("Kasutaja sisestas: " + hinne);
	}

	public static int kuup(int number) {
		return (int) Math.pow(number, 3);

	}

}
