package praktikum6;

import lib.TextIO;

public class LiisuT6mbamine {

	public static void main(String[] args) {
		System.out.println("Kui palju inimesi on?");
		while(true) {
			int i = TextIO.getInt();
			if(i > 0) {
				int j = (int) (Math.random() * i + 1);
				System.out.println("Valisime " + j + " inimese");
				break;
			} else {
				System.out.println("V2hem kui 0 ei saa olla. Proovi uuesti.");
			}
		}

	}

}
