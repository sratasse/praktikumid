package praktikum6;

import lib.TextIO;

public class Kullv6iKiri {

	public static void main(String[] args) {

		// 0 -- kull, 1 -- kiri

		int kasutajaArvab = Meetodid.kasutajaSisestus(0, 1);
		int myndiVise = (Math.random() > 0.5) ? 0 : 1; // lyhike if lause

		System.out.println("Kasutaja sisestas: " + kasutajaArvab);
		System.out.println("Arvuti myndivise: " + myndiVise);

		if (myndiVise == kasutajaArvab) {
			System.out.println("Arvasid 2ra!");
		} else {
			System.out.println("M88da panid");

		}

	}

}