package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class NimiVanus {

	public static void main(String[] args) {
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		
		System.out.println("Sisesta isiku nime ja vanus.");
		
		for (int i=0; i<5; i++ ) {
		
		String nimi = TextIO.getlnString();
		int vanus = TextIO.getlnInt();
		inimesed.add(new Inimene(nimi, vanus));
		}
	

		for (Inimene inimene : inimesed) {
			// Java kutsub välja Inimene klassi toString() meetodi
			System.out.println(inimene);
			
		}
		
	}

}
