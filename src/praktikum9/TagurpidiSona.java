package praktikum9;

import lib.TextIO;

public class TagurpidiSona {

	public static void main(String[] args) {
		String sona;

		System.out.println("Sisesta mingi sõna(palindroom): ");
		sona = TextIO.getlnString();

		String tagurpidi = new StringBuilder(sona).reverse().toString();

		if (sona.equals(tagurpidi)) {
			System.out.println("Need sõnad on samad");
			System.out.println(sona + " = " + tagurpidi);
		} else {
			System.out.println("Need on erinevad sõnad");
			System.out.println(sona + " != " + tagurpidi);
		}
		
		
		/*
		 * public static String tagurpidi(String oigetpidi {
		 * String tagurpidi;
		 * for (int i=0; i<oigetpidi.length(); i++) {
		 * tagurpidi = oigetpidi.charAt(i) + tagurpidi
		 * }
		 * return tagurpidi
		 * }
		 * 
		 * private static boolean on Palindroom (String sona) {
		 * return sona.equals(tagurpidi(sona));
		 *}
		 */

	}
}