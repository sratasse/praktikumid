package testime;

public class Multiarrays {

	public static void main(String[] args) {
		
		int esimene[][] = {{1,2,3,4,5}};
		int teine[][] = {{6,7,8,9,10}};
		
		System.out.println("See on esimene array");
		display(esimene);
		
		System.out.println("See on teine array");
		display(teine);
		
		}

	public static void display(int x[][]){
		for(int rida=0; rida<x.length;rida++){
			for(int column=0; column<x[rida].length; column++){
				System.out.println(x[rida][column] + "\t");
				
			}
			System.out.println();
		}
	}
		
	
}
